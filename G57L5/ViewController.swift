//
//  ViewController.swift
//  G57L5
//
//  Created by Ars on 12.04.2018.
//  Copyright © 2018 ArsenIT. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkStrength(password: "Grand")
        
        
    }
    //Task 1:
    func myName() {
        let name = "Арсен"
        print(name.count)
    }
    
    //Task 2:
    func mySurname(surname: String) {
        if surname.hasSuffix("ич") || surname.hasSuffix("на") {
            print("Ваше отчество заканчивается на ИЧ/НА")
        }
        else {
            print("Ваше отчество не заканчивется на ИЧ/НА")
        }
    }
    
    //Task 3:
    func myNameSurname() {
        var nameSurname = "ArsenIgorevich"
        
        
        print(nameSurname)
    }
    
    //Task 4:
    func reverseName(name: String){
        var reverse = ""
        for character in name.characters {
            var asString = "\(character)"
            reverse = asString + reverse
        }
        print(reverse)
    }
    
    //Task 5:
    func commaLine(){
        
        var str = "1234567838465376459"
        var myCounter: Int = 0
        var newLine: String = ""
        for i in str.reversed() {
            newLine = String(i) + newLine
            str.removeFirst()
            myCounter += 1
            if myCounter == 3 && str != "" {
                newLine = "," + newLine
                myCounter = 0
            }
        }
        print(newLine)
        
    }
    
    //Task 6: Надёжность пароля
    
    func checkStrength(password: String) {
        var strength: Int = 0
        let pattern1 = ["^(?=.*[A-Z]).*$"]
        let pattern2 = ["^(?=.*[a-z]).*$"]
        let pattern3 = ["^(?=.*[0-9]).*$"]
        let pattern4 = ["^(?=.*[!@#%&-_=:;\"'<>,`~\\*\\?\\+\\[\\]\\(\\)\\{\\}\\^\\$\\|\\\\\\.\\/]).*$"]
        
        for pattern in pattern1 {
            if (password.range(of: pattern, options: .regularExpression) != nil) {
                strength += 1
            }
        }
        for pattern in pattern2 {
            if (password.range(of: pattern, options: .regularExpression) != nil) {
                strength += 1
            }
        }
        for pattern in pattern3 {
            if (password.range(of: pattern, options: .regularExpression) != nil) {
                strength += 1
            }
        }
        for pattern in pattern4 {
            if (password.range(of: pattern, options: .regularExpression) != nil) {
                strength += 1
            }
        }
        if strength == 4 {
            strength += 1
        }
        print ("Надёжность пароля равна \(strength)")
    }
    
    
}

